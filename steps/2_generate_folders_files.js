const fs = require('fs')
const path = require('path')

const hds = require('handlebars')
const rimraf = require('rimraf')

const dogs = require('./../output/dogs_list.json')

const resources_path = path.resolve(__dirname, './../resources/')
const dist_path = path.resolve(__dirname, './../dist/')

const template = fs.readFileSync(resources_path + '/template.md').toString()
const hdsTemplate = hds.compile(template)

async function generate() {
  if (!fs.existsSync(dist_path)) {
    fs.mkdirSync(dist_path)
  }

  await new Promise(resolve => {
    rimraf(dist_path + '/*', resolve)
  })

  for (let dog of dogs) {
    if (dog.name.length > 0) {
      const dist_dog_path = dist_path + `/${dog.slug}`
      fs.mkdirSync(dist_dog_path)
      fs.writeFileSync(dist_dog_path + `/dogpage.en.md`, hdsTemplate(dog))
    }
  }
}

module.exports = generate
