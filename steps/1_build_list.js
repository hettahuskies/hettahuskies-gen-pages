const fs = require('fs')
const path = require('path')

const slugify = require('slugify')

function build() {
  let dogs = require('./../input/dogs.json')

  let dogs_list = dogs.map(function(dog, index) {
    let newDoggo = {
      slug: slugify(dog.Name.toLowerCase()),
      name: dog.Name,
      bio: dog.bio,
      more_about_me: dog['Website Text'],
      age: dog['age year'],
      born: dog['Date of Birth'],
      origin: dog['Origin & Farm'],
      microship_number: dog['MicroChip Number'],
      weight: dog['Weight Spring 2018'],
      picture: dog.picture_url,
      alternative_picture: dog['Picture Alternative'],
      gender: dog.Gender.toLowerCase().includes('female') ? 'Female' : 'Male',
      castrated_spayed: dog.Gender.toLowerCase().includes('castrated') ? 'Castrated' : dog.Gender.toLowerCase().includes('spayed') ? 'Spayed' : '',
      lifetime_run_distance: dog['Lifetime Total Distance Run'],
      usual_safari: dog['Maximum Distance'],
      season_rank: dog['2019 Ranking'],
      running_position: dog['Running Position'],
      usual_partner: dog['Good Cobination With'],
      mother_x_father: dog['Mother, Father'] || '',
      // mother_x_father: dog["Mother's Name"] || '' + " x " + dog["Father's Name"] || '',
      siblings: dog['Siblings'],
      usual_diet: dog['Current Food Portion'],
      currently_living: null
    }

    if (dogs.length >= index + 1) {
      newDoggo.next_dog_name = dogs[index + 1] ? dogs[index + 1].Name : null
      newDoggo.next_dog_slug = dogs[index + 1] ? slugify(dogs[index + 1].Name.toLowerCase()) : null
      newDoggo.next_dog_picture = dogs[index + 1] ? dogs[index + 1].picture_url : null
    }

    if (index - 1 >= 0) {
      newDoggo.previous_dog_name = dogs[index + 1] ? dogs[index - 1].Name : null
      newDoggo.previous_dog_slug = dogs[index + 1] ? slugify(dogs[index - 1].Name.toLowerCase()) : null
      newDoggo.previous_dog_picture = dogs[index + 1] ? dogs[index - 1].picture_url : null
    }
    return newDoggo
  })

  fs.writeFileSync(path.resolve(__dirname, './../output/dogs_list.json'), JSON.stringify(dogs_list, null, 2))
}

module.exports = build
