---
title: {{name}}
slug: {{slug}}

taxonomy:
    search: true
    category:
    tag:
    author:

metadata:
    description:
    keywords:
    author:

dog:
  name: {{name}}
  bio: {{bio}}
  picture: {{picture}}
  alternative_pictures:
    - {{alternative_picture}}
  gender: {{gender}}
  more_about_me: {{more_about_me}}
  age: {{age}}
  born: {{born}}
  origin: {{origin}}
  microship_number: {{microship_number}}
  weight: {{weight}}
  castrated_spayed: {{castrated_spayed}}
  lifetime_run_distance: {{lifetime_run_distance}}
  usual_safari: {{usual_safari}}
  season_rank: {{season_rank}}
  running_position: {{running_position}}
  usual_partner: {{usual_partner}}
  mother_x_father: {{mother_x_father}}
  siblings: {{siblings}}
  usual_diet: {{usual_diet}}
  currently_living: {{currently_living}}

next_dog:
  name: {{next_dog_name}}  
  slug: {{next_dog_slug}}  
  picture: {{next_dog_picture}}  
  
previous_dog:
  name: {{previous_dog_name}}  
  slug: {{previous_dog_slug}}  
  picture: {{previous_dog_picture}}  
  
---