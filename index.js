const build = require('./steps/1_build_list')
const generate = require('./steps/2_generate_folders_files')

;(async () => {
  build()
  await generate()
})()
