1. put a dogs.json file from googlesheet (convert it before from csv to json somewhere like here https://www.csvjson.com/csv2json)
2. check that the file has the same properties than in *steps/1_build_list.js* for proper parsing
3. run the script (npm run index.js, need node/npm installed https://nodejs.org/en/download/)